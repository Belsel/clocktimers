using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct ClockEvent 
{
	public UnityEvent clockEvent;
	[Tooltip("If this is not checked, end time will be ignored and the event will only be fired once on the start time")]
	public bool HasEndTime;
	public float StartTime;
	public float EndTime;

}
