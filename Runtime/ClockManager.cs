using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockManager : MonoBehaviour
{

    [Tooltip("The duration of the day in seconds")]
    public float dayDuration = 1440f;
    [Tooltip("How long until the clock checks again to launch events, in seconds.\nCan't be lower than fixedDeltaTime")]
    public float TimePrecision = 1f;
    public float TimePrecisionP { get => _timePrecision; private set { Mathf.Clamp(value, Time.fixedDeltaTime, float.MaxValue); } }

    private float _timePrecision = 1f;

    private float _currentTime;
    public float CurrentTime { get => _currentTime; set { _currentTime = value > dayDuration ? value - dayDuration : value; } }

    private Coroutine _timerCoroutine;
	private List<ClockTimer> _timers = new List<ClockTimer>();


    public static ClockManager Instance;

	void Awake()
	{
		if(Instance == null)
		{
            Instance = this;
		}
        else
		{
            Destroy(gameObject);
		}
		TimePrecisionP = TimePrecision;
	}

	void Start()
	{
		_timerCoroutine = StartCoroutine("ClockTick");
	}

	void OnDestroy()
	{
		if (_timerCoroutine != null)
		{
			StopCoroutine(_timerCoroutine);
		}
	}

	// Update is called once per frame
	void Update()
    {
        CurrentTime += Time.deltaTime;
    }

	/// <summary>
	/// Checks for the time and launches events
	/// </summary>
	/// <returns>Waits for new tick</returns>
	private IEnumerator ClockTick()
	{
		while (true)
		{
			foreach (ClockTimer timer in _timers)
			{
				if (timer.isActiveAndEnabled)
				{
					RunEvents(timer);
				}
			}
			yield return new WaitForSeconds(TimePrecision);
		}
	}

	/// <summary>
	/// Fires all the active events of the given timer.
	/// </summary>
	/// <param name="timer">The given timer</param>
	private void RunEvents(ClockTimer timer)
	{
		foreach (ClockEvent e in timer.Events)
		{
			if (e.HasEndTime)
			{
				if (CurrentTime >= e.StartTime && CurrentTime <= e.EndTime)
				{
					e.clockEvent.Invoke();
				}
			}
			else
			{
				if (CurrentTime >= e.StartTime - (TimePrecision / 2) && CurrentTime < e.StartTime + (TimePrecision / 2))
				{
					e.clockEvent.Invoke();
				}
			}
		}
	}

	/// <summary>
	/// Adds the given timer to the list of events to be fired. Does not need to be manually called.
	/// </summary>
	/// <param name="timer">The given timer</param>
	public void AddTimer(ClockTimer timer)
	{
		_timers.Add(timer);
	}

	/// <summary>
	/// Removes the given timer to the list of events to be fired.
	/// </summary>
	/// <param name="timer">The given timer</param>
	public void RemoveTimer(ClockTimer timer)
	{
		_timers.Remove(timer);
	}
}
