using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockTimer : MonoBehaviour
{	
	public ClockEvent[] Events;

	void Start()
	{
		ClockManager.Instance.AddTimer(this);
	}


	private void OnDestroy()
	{
		ClockManager.Instance.RemoveTimer(this);
	}

	
}
