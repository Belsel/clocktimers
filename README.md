# ClockTimers

## Version
Version 0.0.1

## License
This asset was made by Iván Contreras Guevara (Bélsel)
You can freely use this asset. Please, give credit to me if you are not an EVAD student.
https://evadformacion.com

##
HOW TO USE

1 - Create a gameObject in your scene with a ClockManager component.
2 - Set the day duration and time precision inside the ClockManager component.
3 - Place the ClockTimer component in the gameObjects that you want to listen for a specific time.
4 - Create a new event inside the ClockTimer component and set its start and end times.
5 - Link the event with the action that you want to perform.


## CHANGELOG

[16.1.2023]
- Alpha release